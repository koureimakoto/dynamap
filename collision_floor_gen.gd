extends CollisionPolygon2D

var matrix = [ 10,10,10,16,16,14,10,10,8,8,6,14,16,16,14,10,10,14,10,10]

var tile = 64
var bottom= PoolVector2Array()
var top = PoolVector2Array()
func _init():
	#arr.insert(0, Vector2(0,tile))
	#arr.insert(1, Vector2(tile,tile))
	#arr.insert(2, Vector2(tile * 2,tile) )
	#arr.insert(3, Vector2(tile * 3,tile))
	#arr.insert(4, Vector2(tile * 3,tile * 2))
	
	set_build_mode(0)

	construct_floor_collision(matrix, 10, tile)


func construct_floor_collision(cord: Array, init: int, tile_size: int) -> int:
	var id : int = 0
	var count : int    = 1
	var size  : int    = cord.size()
	var prev  : int    = cord[0]
	var next  : int    = cord[1]
	var walk  : Array  = [false, false]
	var walk_count     = [0,0]
	
	if size <= 0: return -1
	
	top.insert(0, Vector2(10 * tile, tile ))
	#bottom.insert(0, Vector2(init.x, init.y - tile_size))
	
	while count < size -1:
		if   next > prev:
			top.insert(id, Vector2( count * tile + tile, (prev - init) * tile ))
			top.insert(id+1, Vector2( count * tile + tile, (prev - next) * tile))
			id += 1
		elif next < prev:   
			top.insert(id  , Vector2( count * tile + tile, prev * tile))
			top.insert(id+1, Vector2( count * tile + tile, (prev - next + next)  * tile))
			id += 1
			
		else:
			print('igual')
		
		
		print('id ', count * tile + tile)
		
		#id += 1
		count += 1
		prev = next
		next = cord[count]
		
	
	print('top ', top)
	print('prev ', prev)
	print('next ', next)
	#count = 0   
	#while count < size:
	#    bottom.insert(count, Vector2( count * tile +64, tile * 2))
	#    count += 1
	
	#count = 0
	#while count < size:
	#    top.insert( count + size , bottom[size-count-1])
	
   #     count += 1 
   #     pass

	set_polygon(top)
	return 0
