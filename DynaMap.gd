extends Node2D

var loading = load("res://mode_0.tscn")     ; var temp_simbol : int = 9 ;
var d_stack = load("res://render_stack.gd") ;
var tilemap = load("res://tile_map_dyna.gd")

enum mapping_coord {
	NW= 0, N = 1, NE= 2,
	W = 3, CT= 4, E = 5,
	SW= 6, S = 7, SE= 8}

# Constants
const MAX_UINT_32 = 0xFFFFFFFF
const PERIOD      = 0.2  # Periodo de ativacao do tempo
const redution    = 0.69  # Variavel que define a area de reducao proporcional dos retangulos
var   time        = 0     # Contador de tempo



var gen_data ={
	'size'  : {'x': 29, 'y': 16},
	'tile'  : 64,
	'map'   : []
}


#Floor Object Array
var floor_data = {
	'level'             : 10,
	'player_init_space' : 5,
	'map'               : [],
	'conditions'        : {
		'rise':{ 'max': 0, 'min': 0 , 'weight':2 , 'space_between' : 0, 'floating_floor' : false},
		'deep':{ 'max': 1, 'min': 0.5 , 'weight':2 , 'space_between' : 0, 'erosion'        : false},
		'gen_rise_lock': false
	}
}

var row_size      = Vector2(
	gen_data.tile - (gen_data.tile - int(gen_data.tile * redution )),
	gen_data.tile - (gen_data.tile - int(gen_data.tile * redution )))
var column_size   = Vector2(
	gen_data.tile - (gen_data.tile - int(gen_data.tile * (redution + 0.15))),
	gen_data.tile - (gen_data.tile - int(gen_data.tile * (redution + 0.15))))

var column_render = Vector2(6,6)
var row_pos      : Array = [0,0]




func _ready():
	d_stack = d_stack.new()
	tilemap = tilemap.new()
	
	d_stack.mount_draw_stack(self, [
		'draw_animated_column',
		'draw_tile_row',
		'draw_grid'
	])
	
	
func draw_grid():
	var count = {'x':0,'y':0}
	var size = 0
	var w = OS.get_screen_size().x
	var h = OS.get_screen_size().y
	while count.x < w :
		draw_line(Vector2(count.x, 0), Vector2(count.x, h), Color(1,0,0))
		count.x += gen_data.tile
	while count.y < h : 
		draw_line(Vector2(0, count.y), Vector2(w, count.y), Color(1,0,0))
		count.y += gen_data.tile
# Prepara a estrutura que sera um unico bloco de tile
# Substituir depois o loading por um inteiro 
func create_row():
	# Apenas para teste, sera substituido por Sprite, Colisao se calculadas a parte.
	var numb = Sprite.new();

	var floor_unit = {
		'obj'      : numb,
		'value'    : -1,
		'condition': []
	}
	return floor_unit;

func insert_floor_column(x: int) -> void:
	floor_data.map.insert(x, 0)

func link_floor_to_gen(x: int):
	floor_data.map[x] += 1

func check_tile_value(x: int, y: int) -> int:
	var pos = 1
	
	var center = get_gen_map_value(x,y)
	var left   = get_gen_map_value(x-pos, y)
	var top    = get_gen_map_value(x, y-pos)
	var right  = get_gen_map_value(x+pos, y)
	var bottom = get_gen_map_value(x, y+ pos)
	
	if center ==  9: 
		center = ( 2 if left == -1 else 3) + ( 1 if right == 9 else 2 )
	if bottom == -1: center += 3
	elif top == -1:  center -= 3
	
	gen_data.map[x][y].value = center
	gen_data.map[x][y].obj = tilemap.get_tile(0, center)
	gen_data.map[x][y].obj.position.x = x * gen_data.tile + gen_data.tile/2
	gen_data.map[x][y].obj.position.y = y * gen_data.tile + gen_data.tile/2
	add_child(gen_data.map[x][y].obj)
	print(gen_data.map[x][y].obj.position)
	return center;

func get_gen_map_value(x: int, y: int) -> int:
	return int(gen_data.map[x][y].value)




# insera uma nova coluna que ira receber as linhas
func insert_column(x: int) -> void:
	gen_data.map.insert(x, [])
	
# insere uma nova linha na coluna que sera criada
func insert_row(x: int, y: int) -> void:    
	gen_data.map[x].insert(y, create_row())
	#gen_data.map[x][y].obj.position.x = x * gen_data.tile + gen_data.tile/2
	#gen_data.map[x][y].obj.position.y = y * gen_data.tile + gen_data.tile/2
	
	#gen_data.map[x][y].obj.node.set_text('-1')
	


# GERADOR DE ELEVACAO DO TERRENO DE FORMA RANDOMICA ----------------------------
#
# funtion( x ; y ; weight ) : void
#   x > Valor X da matriz, ou ID da coluna
#   y > Valor Y da matriz, ou ID da linha
#   weight > Peso que terminara o tamanho do intervalo sempre > floor_obj.level
#
# Esta funcao segue como intuito gerar um intervalo entre MIN_UINT_32 e MAX com 
# isso utilizar sua posicao em Y para determinar o intervalo de precisao para o
# acerto do numero randomico em relacao a abertura (hole)

func floor_gen_rise(x : int, y : int):
	if x < floor_data.player_init_space: return; # Apenas encerra a execucao.
	 
	# 0x0 ate 0xFFFF_FFFF
	randomize(); 
	var gen    = randi();
	var weight = floor_data.conditions.rise.weight
	var hole  = (MAX_UINT_32 / y ) * weight
	
	# Valor Anterior
	y -= 1 
	var value = gen_data.map[x][y+1].value
	
	# Se estiver no intervalo && o valor anterior for verdadeiro
	if (gen < hole) && (value == temp_simbol):
		#gen_data.map[x][y].obj.set_text(temp_simbol)
		gen_data.map[x][y].value = 9
		link_floor_to_gen(count.x-2)
	
		
		
# GERADOR DE BURACOS DE FORMA RANDOMICA ----------------------------------------
#
# funtion( x ; y ; weight ) : void
#   x > Valor X da matriz, ou ID da coluna
#   y > Valor Y da matriz, ou ID da linha
#   weight > Peso que terminara o tamanho do intervalo sempre > floor_obj.level
#
# Esta funcao segue como intuito gerar um intervalo entre MIN_UINT_32 e MAX com 
# isso utilizar sua posicao em Y para determinar o intervalo de precisao para o
# acerto do numero randomico em relacao a abertura (hole)
	   
func floor_gen_deep(x : int, y : int ):
	if x < floor_data.player_init_space:
		#gen_data.map[x][y].obj.set_text(temp_simbol) ; 
		gen_data.map[x][y].value = 9
		link_floor_to_gen(count.x-2); return;
	
	randomize();               #Randomize Seed by Time
	var gen    = randi(); # 0x0 at 0xFFFF_FFFF
	var weight = floor_data.conditions.deep.weight
	
	var hole        = (MAX_UINT_32 / y) * weight # Interval Size 
	var value       = gen_data.map[x][y-1].value
	var value_level = gen_data.map[x][floor_data.level-1].value
	
	if ( gen < hole ) && (value != temp_simbol):
		#gen_data.map[x][y].obj.set_text('-1')
		gen_data.map[x][y].value = -1
	else:
		#gen_data.map[x][y].obj.set_text(temp_simbol)
		gen_data.map[x][y].value = 9
		link_floor_to_gen(count.x-2)
	print(gen_data.map[x][y].value)

		





# DRAW
func draw_tile_row(arr:Array) -> void:
	draw_rect( # Desenho da Linha
		Rect2( Vector2(arr[0], arr[1]), row_size ),
		Color(0.6,0.2,0.2), true
		)
		
func draw_animated_column() -> void:
	draw_rect( # Desenho da Coluna
		Rect2(
			Vector2(column_render.x,(gen_data.tile - column_size.x)/2) ,
			Vector2(column_size.x, column_render.y)),
		Color(0.6,0.4,0.4), true
		)

# CALC
func calc_animated_column(x: float, y: float):
	column_render = Vector2(
		(x * gen_data.tile) + (gen_data.tile- column_size.x)/2,
		(y * gen_data.tile) 
	)
	
func calc_tile_row(arr: Array, x: float, y: float) -> void:
	arr[0] = (x * gen_data.tile + (gen_data.tile - row_size.y)/2) 
	arr[1] = (y * gen_data.tile + (gen_data.tile - row_size.y)/2) 




var count = {'x':0, 'y':0}
func prepare_column(x: int, y: int) -> void:
	insert_column(x);
	while(y < gen_data.size.y):
		insert_row(x, y); y += 1;
		
	calc_animated_column(x, y)
	d_stack.to_draw([], d_stack.draw_stack_enum.draw_animated_column)
   
func floor_gen(x: int, y: int) -> void:
	#if x == 0 || x == gen_data.size.x -1 : return;
	y = floor_data.level;
	insert_floor_column(x-1)
	while y < gen_data.size.y - 1 :
		floor_gen_deep(x, y); y += 1
		
	y = floor_data.level;
	while y > 0 :
		floor_gen_rise( x, y ); y -= 1
		
		
var tile_floor_gen_count = {'x':1 ,'y':0}
func tile_floor_gen(x: int, y: int) -> void:
	var size = gen_data.size.y - 1
	while( y < size ):
		check_tile_value(x, y)
		y += 1
	
		
		


func hide_column() -> void:
	count.y = 0
	while(count.y < gen_data.size.y):
		remove_child(gen_data.map[count.x][count.y].obj)
		count.y += 1

func unhide_column() -> void:
	count.y = 0
	while(count.y < gen_data.size.y):
		add_child(gen_data.map[count.x][count.y].obj)
		count.y += 1

func free_column() -> void:    
	count.y = 0
	while(count.y < gen_data.size.y):
		gen_data.map[count.x][count.y].obj.queue_free()
		count.y += 1
	

func _draw():
	d_stack.draw_in_stack()
	

func _process(delta):
	time += delta
	if time > PERIOD:
		if count.x < gen_data.size.x:
			count.y = 0
			prepare_column(count.x, count.y)
		if count.x < gen_data.size.x && count.x > 1:
			floor_gen(count.x - 1, count.y)
		if count.x < gen_data.size.x+1 && count.x > 2:
			tile_floor_gen(count.x - 2, gen_data.size.y - floor_data.map[count.x-3] -1 )
			# create_floor_colision
			pass
		count.x += 1

		d_stack.to_draw([], d_stack.draw_stack_enum.draw_grid)   
		update()
		time = 0


#*******************************************************************************

