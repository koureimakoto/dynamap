#extends "res://DynaMap.gd"

# ---------------------------------------------------------------------------- #
# PILHA DE RENDER

# ENUM para adicionar quais funcoes de sao elegiveis para o render
#   Usar prefix draw na frente delas para nao haver confunsao com as funcoes de 
# calculo.
#   Para evitar que o enum defina implicitamente qual o primeiro numero adicione
# [0] ao primeiro elemento. Pois sera necessario para percorrer um vetor.
enum draw_stack_enum {
    draw_animated_column = 0,
    draw_tile_row,
    draw_grid
}

#   STRUCT que contem o como sera renderizado. O size e reduntante para controle
# se auto incrementa, mas no conjunto de funcoes nao esta sendo utilizado. Mais 
# para estatistica e debbug.
var draw_stack = {
    'size'  : 0,
    'func'  :[],
    'render':[]
}

# MOUNT_draw_stack
# Monta e prepara a stack de funcoes que serao executadas depois by ID 
func mount_draw_stack(obj: Object, name : Array) -> void:
    var size : int     = name.size()
    var count: int     = 0
    var temp : FuncRef = null # Tipo Referencia de Funcao manual em @GDScript
    while(size > count):
        temp = funcref( obj , name[count]) #Cria uma ref da funcao por nome
        draw_stack.func.insert(count, temp)
        temp = null ; count += 1;

# TO_DRAW
# funcao que prepara a stack de render, cada chamada dela adiciona na pilha a 
# a funcao, mesmo que ela seja chamada novamento, colocara em um novo espaco.
#   A funcao recebe por padrao um array que deve receber os parametros na sequen
# quencia do escopo da funcao. Se precisar usar Referencia, passar como Array,
# Dicionario ou Instancia de Objeto. Qualquer outro tipo será tratado como Valor.
func to_draw(arr: Array, draw_id : int) -> void:
    draw_stack.render.push_back([draw_id, arr])

# DRAW_IN_STACK
#    Funcao especial que renderizar por stack todos os processos de callback e 
# como eles esta pre alocado por id, nao precisa ter uma busca para realizar a 
# renderizacao, so cuidado que ele e uma pilha, entao isso vai depender da ordem
# de execucao do TO_DRAW, isso quer dizer e que a prioridade e defina por ordem
# de chamada.
# Isso para evitar consumo de recurso com sort. Da para fazer de outra forma, 
# mas recomendo que seja feito em C/CPP ou RUST, pois usar busca ira comprometer
# muito a performance em GDscript para fazer operacoes basicas.
func draw_in_stack() -> void:
    # Pega do final e remove. Esta Stack sera montada a cada ciclo e precisa 
    # ser limpa apos o uso completo. Se estiver vazio, retorna nulo, isso quer
    # dizer que so sera chamada em um novo ciclo de processo da godot.
    var value = draw_stack.render.pop_back()
    
    while(value != null):
        # chama a funcao por id[0], o que economiza busca e passa o parametro[1]
        draw_stack.func[value[0]].call_funcv(value[1])
        value = draw_stack.render.pop_back()

# FIM DA PILHA DE RENDER
# ---------------------------------------------------------------------------- #
