extends KinematicBody2D

onready var node


# Called when the node enters the scene tree for the first time.
func _init():
	node = RichTextLabel.new()
	node.margin_bottom = 58
	node.margin_top = -6
	node.margin_right = 60
	node.margin_left = -4
	node.set_position(Vector2(-4,-6))
	node.set_size(Vector2(64,64))
	add_child(node)
	pass   
 #node.set_text(num)
	

func set_text(text:String):
	self.node.set_text(text)
	if text != '-1':
		node.set_override_selected_font_color(true)
		node.set_modulate(Color(0.9,0.2,0.2))
	if text == '9':
		node.set_override_selected_font_color(true)
		node.set_modulate(Color(0,0.8,0))
	
func get_text():
	return node.get_text()

