extends Node2D

enum type {
	EXTERN_3X3 = 0,
	INTERN_3x3 = 1
}


var img = Image.new()
var  image = ImageTexture.new()
var  sprite = Sprite.new()

var img_count: int = -1
var img_path : String

var tilemaps : Array


func _init():
	var id = create_tilemap('firt_map', "res://map.png", Vector2(192,192))
	create_mask(id, Vector2(0,0), Vector2(64,64), 0)
	print_sprite(id)
	#img.load("res://map.png")
	#var x = img.get_rect(Rect2(Vector2(0,0),Vector2(64,64)))
	
	#print(x)
	#print(image.create_from_image(x))
	#print(sprite.set_texture(image))
	#sprite.set_position(Vector2(640,640))
	#add_child(sprite, true)
	print(print_tree_pretty())
	
	
func create_tilemap(name: String, path: String, size: Vector2, flag : int = 4) -> int:
	var id : int   = img_count + 1
	var img: Image = Image.new()
	
	img.load(path)
	
	var map : Dictionary = {
		'name': name,
		'raw' : img,
		'mask': [[],[],[],[],[],[],[],[],[]]
	}
	
	tilemaps.insert(id, map)
	
	return id
	
func create_mask(id: int, pos: Vector2, size: Vector2, type: int) -> int:
	var mask_id : int
	
	var count = 0
	while count < 3:
		var sprite = [Sprite.new(),       Sprite.new(),       Sprite.new()]
		var texture= [ImageTexture.new(), ImageTexture.new(), ImageTexture.new()]
		
		texture[0].create_from_image(tilemaps[id].raw.get_rect(Rect2(Vector2(pos.x,              pos.y + (size.y * count)), size)))
		texture[1].create_from_image(tilemaps[id].raw.get_rect(Rect2(Vector2(pos.x + size.x    , pos.y + (size.y * count)), size)))
		texture[2].create_from_image(tilemaps[id].raw.get_rect(Rect2(Vector2(pos.x + size.x * 2, pos.y + (size.y * count)), size)))
		
		sprite[0].set_texture( texture[0])
		sprite[1].set_texture( texture[1])
		sprite[2].set_texture( texture[2])
	
		tilemaps[id].mask[    (count * 3)].insert(0, sprite[0]) 
		tilemaps[id].mask[1 + (count * 3)].insert(0, sprite[1]) 
		tilemaps[id].mask[2 + (count * 3)].insert(0, sprite[2]) 
		
		#add_child( tilemaps[id].mask[(count * 3)][0], true)
		#add_child( tilemaps[id].mask[1 + (count * 3)][0], true)
		#add_child( tilemaps[id].mask[2 + (count * 3)][0], true)
		count += 1

	
	return mask_id

func print_sprite(id):
	var x = 32
	tilemaps[id].mask[0][0].set_position(Vector2(x+ 0,x+0))
	tilemaps[id].mask[1][0].set_position(Vector2(x+64,x+0))
	tilemaps[id].mask[2][0].set_position(Vector2(x+128,x+0))
	tilemaps[id].mask[3][0].set_position(Vector2(x+0,x+64))
	tilemaps[id].mask[4][0].set_position(Vector2(x+64,x+64))
	tilemaps[id].mask[5][0].set_position(Vector2(x+128,x+64))
	tilemaps[id].mask[6][0].set_position(Vector2(x+0,x+128))
	tilemaps[id].mask[7][0].set_position(Vector2(x+64,x+128))
	tilemaps[id].mask[8][0].set_position(Vector2(x+128,x+128))
	print('ele:: ',tilemaps[id].raw)

func get_tile(id: int, pos: int):
	return tilemaps[id].mask[pos][0].duplicate()
